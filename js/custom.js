//Home page box left right equal height
$(window).load(function () {
    equalheight = function (container) {
        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = new Array(),
            $el,
            topPosition = 0;
        $(container).each(function () {

            $el = $(this);
            $($el).height('auto')
            topPostion = $el.position().top;

            if (currentRowStart != topPostion) {
                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    };
});
$(window).load(function () {
    equalheight('.equal-main .equalh, .media-en-block .col-md-4');
});
$(window).resize(function () {
    equalheight('.equal-main .equalh, .media-en-block .col-md-4');
});

// jquery document ready
$(document).ready(function () {
    // navbar-toggle
    $('.header .navbar-toggle').click(function (event) {
        var ariaexpanded = $(this).attr('aria-expanded');
        if (ariaexpanded === 'true') {
            $(this).children('.material-icons').text('menu');
        }
        else {
            $(this).children('.material-icons').text('close');

        }
        // event preventDefault
        event.preventDefault();
    });

    // move-to section
    $('.move-to').click(function (event) {
        var goto = $(".mobile-connectivity").offset().top;
        $('html,body').animate({'scrollTop': goto}, 500, 'swing');
        // event preventDefault
        event.preventDefault();
    });

});

